<?php
defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {

    $settings = new admin_settingpage('local_hfl', 'Настройки HuntFlow Link');
    $ADMIN->add('server', $settings);


    $settings->add(new admin_setting_configcheckbox(
        'local_hfl/active',
        'Активно', // имя
        'Передавать данные', // описание
        0 // значение по умолчанию
    ));

    $settings->add(new admin_setting_configtext(
        'local_hfl/url',
        // Дружественный title для конфига, который будет показан
        'Ссылка на внешний API',
        // Поясняющий текст для поля конфига
        'Ссылка на внешний сервис',
        // Значение по умолчанию
        '',
        // Тип параметра этого поля конфига
        PARAM_TEXT
    ));

    $settings->add(new admin_setting_configtext(
        'local_hfl/apikey',
        'Ключ к внешнему API',
        'Данный ключ будет использоваться для авторизации во внешнем API',
        '',
        PARAM_TEXT
    ));
}