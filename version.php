<?php
// https://docs.moodle.org/dev/version.php
defined('MOODLE_INTERNAL') || die();

$plugin->component = 'local_hfl';
$plugin->maturity = MATURITY_ALPHA;
$plugin->release = '0.0.1';
$plugin->requires = 2019052000;
$plugin->version = 30042021;
