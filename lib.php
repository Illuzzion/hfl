<?php

class send_data_adhoc_task extends \core\task\adhoc_task
{
    public function execute()
    {
        // Получим url сервиса принимающего результаты
        $config = get_config('local_hfl');

        $url = (string)$config->url;
        if (!$url) return;

        // http или https
        $scheme = parse_url($url, PHP_URL_SCHEME);

        // готовим payload запроса
        $data = $this->get_custom_data();
        $postdata = json_encode($data, JSON_UNESCAPED_SLASHES);

        $opts = array($scheme =>
            array(
                'method' => 'POST',
                'header' => 'Content-Type: application/json' . PHP_EOL,
                'content' => $postdata
            )
        );

        // если указан api key, добавим его в header
        $apikey = (string)$config->apikey;
        if ($apikey) {
            $opts[$scheme]['header'] .= 'Authorization: Bearer ' . $apikey;
        }

        $context = stream_context_create($opts);

        $response = @file_get_contents(
            $url,
            false,
            $context
        );

        // TODO: Нужно отслеживать неудачные попытки запроса
        // и писать логи
        if (empty($response)) return false;
        return true;
    }
}

/* Обработчик, добавляет задания в очередь */
function handler($event)
{
    // https://moodle.org/mod/forum/discuss.php?d=199867
    $config = get_config('local_hfl');

    $active = (bool)$config->active;
    if (!$active) return;

    // массив с хуками, которые перехватываем
    $hook_events = array(
        '\core\event\user_created',
        '\core\event\role_assigned',
        '\core\event\user_enrolment_created',

        '\core\event\user_loggedin',
//        '\core\event\user_loggedout',
//        '\core\event\user_deleted',

        '\tool_policy\event\acceptance_created',

        '\core\event\course_viewed',
        '\core\event\course_completed',
        '\core\event\course_module_completion_updated',

        '\mod_lesson\event\essay_attempt_viewed',
        '\mod_quiz\event\attempt_started',
        '\mod_quiz\event\attempt_summary_viewed',
        '\core\event\user_graded',
        // test
        '\core\event\dashboard_viewed'
    );

    $data = $event->get_data();
    $tracked_event = in_array($data['eventname'], $hook_events);

    if (
    array_key_exists('eventname', $data)
//        && $tracked_event
    ) {
        $task = new send_data_adhoc_task();

        $task->set_custom_data($data);
        \core\task\manager::queue_adhoc_task($task);

        $success = $task->execute();

        if (!$success) {
            // логируем неудачные попытки отправки события
            file_put_contents(
                __DIR__ . '/error.log',
                date("d.m.y H:i:s") . ' data not submitted: ' . $data['eventname'] .
                ', user_id ' . $data['userid'] . PHP_EOL,
                FILE_APPEND
            );

//            $future_time = time() + 20;
//            $task->set_next_run_time($future_time);
//            \core\task\manager::reschedule_or_queue_adhoc_task($task);
        }
    }
}