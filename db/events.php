<?php
defined('MOODLE_INTERNAL') || die();

$observers = array(
    array(
        'callback'    => 'handler',
        'eventname'   => '*',
        'includefile' => '/local/hfl/lib.php',
        'internal'    => true,
        'priority'    => 200,
    ),
);